# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
import datetime
from w1.signals import transaction_paid

class Order(models.Model):
    user = models.ForeignKey(User, verbose_name=u'Плательщик')
    created = models.DateTimeField(u'Создана', auto_now_add=True)
    paid = models.DateTimeField(u'Оплачена', blank=True, null=True)
    purse = models.CharField(u'Кошелек', max_length=12, blank=True, null=True)
    order_id = models.CharField(u'# заказа', max_length=12, blank=True, null=True)
    amount = models.FloatField(u'Сумма')
    
    def pay(self, purse, order_id):
        self.purse = purse
        self.order_id = order_id
        self.paid = datetime.datetime.now()
        self.save()
        transaction_paid.send(sender=self)
        
    def is_paid(self):
        return self.paid is not None
