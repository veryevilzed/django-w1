# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
import hashlib
import base64

class W1Form(forms.Form):
    WMI_MERCHANT_ID = forms.IntegerField(widget=forms.HiddenInput(),
                                      initial=settings.W1_MERCHANT_ID)
    WMI_PAYMENT_AMOUNT = forms.CharField(widget=forms.HiddenInput())
    WMI_CURRENCY_ID = forms.IntegerField(widget=forms.HiddenInput(),
                                      initial=643)
    WMI_DESCRIPTION = forms.CharField(widget=forms.HiddenInput())
    WMI_SUCCESS_URL = forms.CharField(widget=forms.HiddenInput(),
                                      initial=settings.W1_SUCCESS_URL)
    WMI_FAIL_URL = forms.CharField(widget=forms.HiddenInput(),
                                   initial=settings.W1_FAIL_URL)
    WMI_PAYMENT_NO = forms.CharField(widget=forms.HiddenInput())
    
    WMI_SIGNATURE = forms.CharField(widget=forms.HiddenInput())
    
    def set_signature(self):
        # Параметр WMI_SIGNATURE формируется путем объединения значений 
        # всех остальных параметров формы в алфавитном порядке их имен 
        # (без учета регистра) с добавлением в конец «секретного ключа» 
        # интернет-магазина. Если форма содержит несколько полей с 
        # одинаковыми именами, такие поля сортируются в алфавитном порядке 
        # их значений.
        # Полученое после объединения параметров и «секретного ключа» 
        # значение, представленное в кодировке Windows-1251, хешируется 
        # выбранным методом формирования ЭЦП и его байтовое представление 
        # кодируется в Base64.
        f = []
        for k in self.fields:
            if k == 'WMI_SIGNATURE':
                continue
            v = self.initial.get(k, None) or self.fields[k].initial
            f.append((k, str(v).decode('utf-8').encode('cp1251')))
        f = sorted(f, key=lambda field: str.lower(field[0]))
        s = ''.join([k[1] for k in f])
        s+=settings.W1_SECRET_KEY
        key = base64.b64encode(hashlib.sha1(s).digest())
        self.initial['WMI_SIGNATURE'] = key